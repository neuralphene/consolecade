#!/bin/bash

## This script should be run on a fresh install of RetroPie. It will set it up
## as a consolcade. 
## Assumptions: filesystem has been expanded (sudo raspi-config) and sources has
## been updated (sudo apt-get update)

## ensure this is run as root
if [[ $EUID -ne 0 ]]; then
	echo "Must be run as root user. Try again with sudo"
	exit 1
fi

## Create Symlinks for configuration files
mv /etc/emulationstation/es_systems.cfg /etc/emulationstation/es_systems.cfg.orig
ln -s /home/pi/consolecade/config/es_systems.cfg /etc/emulationstation/es_systems.cfg

mv /home/pi/.emulationstation /home/pi/.emulationstation.orig
ln -s /home/pi/consolecade/emulationstation /home/pi/.emulationstation

ln -s /home/pi/consolecade/config/gpsp.cfg /opt/retropie/emulators/gpsp/gpsp.cfg

mv /opt/retropie/emulators/pisnes/snes9x.cfg /opt/retropie/emulators/pisnes/snes9x.cfg.orig
ln -s /home/pi/consolecade/config/snes9x.cfg /opt/retropie/emulators/pisnes/snes9x.cfg

mv /opt/retropie/emulators/retroarch/configs /opt/retropie/emulators/retroarch/configs.orig
ln -s /home/pi/consolecade/config/controllers /opt/retropie/emulators/retroarch/configs

mv /opt/retropie/configs /opt/retropie/configs.orig
ln -s /home/pi/consolecade/config/retroarch /opt/retropie/configs

mv /opt/retropie/emulators/retroarch/retroarch.cfg /opt/retropie/emulators/retroarch/retroarch.cfg.orig
ln -s /home/pi/consolecade/config/retroarch.cfg /opt/retropie/emulators/retroarch/retroarch.cfg

## update to show motd & direct output to null
cat /etc/profile | grep -v emulationstation > /tmp/profile
echo "[ -n \"\${SSH_CONNECTION}\" ] || /home/pi/consolecade/motd/show.sh" >> /tmp/profile
echo "[ -n \"\${SSH_CONNECTION}\" ] || emulationstation > /dev/null 2>&1" >> /tmp/profile
mv /tmp/profile /etc/profile

##  update splash screen
rm /etc/splashscreen.list
echo "/home/pi/consolecade/splash/splash.png" > /tmp/splashscreen.list
mv /tmp/splashscreen.list /etc/splashscreen.list


## @TODO setup Adafruit-retrogame (i.e. if modules are required, software installed, etc)


## start Adafruit-retrogame at boot (added to crontab)
echo "@reboot /home/pi/consolecade/retrogame > /dev/null 2>&1" > /tmp/crontab
mv /tmp/crontab /var/spool/cron/crontabs/root

## enable USB rom service
cp /home/pi/RetroPie-Setup/supplementary/01_retropie_copyroms /etc/usbmount/mount.d/
sed -i -e "s/USERTOBECHOSEN/$USER/g" /etc/usbmount/mount.d/01_retropie_copyroms
chmod +x /etc/usbmount/mount.d/01_retropie_copyroms

