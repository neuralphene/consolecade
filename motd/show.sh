#!/bin/bash

#dependent on aspect ratio

#16:10, show full res
#16:9, cut off bottom
#4:3, cut off left

let columns="$(tput cols)"
let lines="$(tput lines)"
let coldiff=240-$columns+1

#echo "lines  : ${lines}"
#echo "columns: ${columns}"
#echo "coldiff: ${coldiff}, lines: ${lines}"

w=240
h=75

if [ "$lines" -ge "65" ]; then
	h=75
elif [ "$lines" -ge "55" ]; then
	h=64
else
	h=50
fi

file="/home/pi/consolecade/motd/motd.${w}x${h}"

head -n${lines} ${file} | cut -c${coldiff}-240
